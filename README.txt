***********
* README: *
***********

DESCRIPTION:
------------
This module provides a wrapper function to ImageAPI that adds reflections to images.


REQUIREMENTS:
-------------
The imageapi_reflect.module requires the imageAPI.module to be installed.


INSTALLATION:
-------------
1. Place the entire email directory into your Drupal sites/all/modules/
   directory.

2. Enable the email module by navigating to:

     administer > modules


FEATURES:
---------
* adds reflections to images
* allows background relection color to be set
* allows height of reflection to be set
* allows for beginning and ending of gradient to be set


AUTHOR:
-------
Jason P Salter
jason@fivepaths.com
http://fivepaths.com