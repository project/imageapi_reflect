<?php

function imageapi_reflect_form($data) {

  $form['rgb'] = array(
    '#type' => 'textfield',
    '#title' => t('Background color in RGB'),
    '#default_value' => $data['rgb'] ? $data['rgb'] : '0,0,0',
    '#description' => t('Enter a RGB values separated by commas (0 <= values <= 255) : default 0,0,0'),
  );
  $form['output_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Height of reflection as ratio of image height'),
    '#default_value' => $data['output_height'] ? $data['output_height'] : '.35',
    '#description' => t('0 < value < 1 : default .35'),
  );
  $form['alpha_start'] = array(
    '#type' => 'textfield',
    '#title' => t('Alpha start value'),
    '#default_value' => $data['alpha_start'] ? $data['alpha_start'] : '80',
    '#description' => t('0 < value < 100 : default 80'),
  );
  $form['alpha_end'] = array(
    '#type' => 'textfield',
    '#title' => t('Alpha end value'),
    '#default_value' => $data['alpha_end'] ? $data['alpha_end'] : '0',
    '#description' => t('0 < value < 100: default 0'),
  );

  return $form;
}

function theme_imageapi_reflect($element) {
  $data = $element['#value'];
  return 'RGB: '. $data['rgb'] .', Output height: '. $data['output_height'] .', Alpha start: '. $data['alpha_start'] .', Alpha end: '. $data['alpha_end'];
}

function imageapi_reflect_image(&$image, $data = array()) {
  if (!imageapi_reflect_image_reflect($image, $data)) {
    watchdog('imagecache', t('imagecache_reflect failed. image: %image, data: %data.', array('%path' => $image, '%data' => print_r($data, TRUE))), WATCHDOG_ERROR);
    return FALSE;
  }
  return TRUE;
}